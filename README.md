# Personal Hugo Theme

This is the theme that I've developed for my personal website [alexselimov.com](https://alexselimov.com). 
Reach out to me if you have any questions or if you need help adjusting it to your needs.
