/* Toggle between adding and removing the "responsive" class to topnav when the user clicks on the icon */
function myFunction() {
    var x = document.getElementById("Nav");
    if (x.className === "primary") {
      x.className += " responsive";
    } else {
      x.className = "primary";
    }
    var x = document.getElementById("Ham");
    if (x.className === "hamburger") {
      x.className += " responsive";
    } else {
      x.className = "hamburger";
    }

    var children = x.children;
     if (children[0].className === "bars") {
      children[0].className += " responsive";
    } else {
      children[0].className = "bars";
    }     
     if (children[1].className === "close") {
      children[1].className += " responsive";
    } else {
      children[1].className = "close";
    }     

    var x = document.getElementById("Navmobile");
    if (x.className === "mobile") {
      x.className += " responsive";
    } else {
      x.className = "mobile";
    }

}

function unshow_all(id){
    var x = document.getElementById("About");
    x.className = "";
    var x = document.getElementById("Ministries");
    x.className = "";
    var x = document.getElementById("Give");
    x.className = "";
    var x = document.getElementById("Resources");
    x.className = "";
}
function show(id){
    var x = document.getElementById(id);
    if(x.className === "show"){
        var needshow = 0;
    }
    else{
        var needshow= 1;
    }
    unshow_all(id);
    if ( needshow === 1 ){
        x.className = "show";
    }
}
